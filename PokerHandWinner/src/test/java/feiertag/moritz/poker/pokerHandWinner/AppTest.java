package feiertag.moritz.poker.pokerHandWinner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import java.util.Set;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import feiertag.moritz.poker.pokerHandWinner.exceptions.DuplicateCardsDetected;
import feiertag.moritz.poker.pokerHandWinner.exceptions.InvalidNumerOfCardsException;


/**
 * Unit test for <code>feiertag.moritz.poker.pokerHandWinner.determineWinner(Set&lt;Card&gt;, Set&lt;Card&gt;)</code>.
 */
public class AppTest 
{

    //------ Different Rank Tests ------//
	
    @Test
    @DisplayName("Straight Flush against four of a kind")
    public void straightFlush_FourOfAKind() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.FOUR_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Straight Flush against full house")
    public void straightFlush_FullHouse() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.FULL_HOUSE);
    }
    
    @Test
    @DisplayName("Straight Flush against flush")
    public void straightFlush_Flush() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.FLUSH);
    }
    
    @Test
    @DisplayName("Straight Flush against Straight")
    public void straightFlush_Straight() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.STRAIGHT);
    }
    
    @Test
    @DisplayName("Straight Flush against three of a kind")
    public void straightFlush_ThreeOfAKind() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.THREE_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Straight Flush against two pairs")
    public void straightFlush_TwoPairs() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.TWO_PAIRS);
    }
    
    @Test
    @DisplayName("Straight Flush against single Pair")
    public void straightFlush_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Straight Flush against high card")
    public void straightFlush_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Four of a kind against full house")
    public void fourOfAKind_FullHouse() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.FULL_HOUSE);
    }
    
    @Test
    @DisplayName("Four of a kind against flush")
    public void fourOfAKind_Flush() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.FLUSH);
    }
    
    @Test
    @DisplayName("Four of a kind against straight")
    public void fourOfAKind_Straight() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.STRAIGHT);
    }
    
    @Test
    @DisplayName("Four of a kind against three of a kind")
    public void fourOfAKind_ThreeOfAKind() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.THREE_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Four of a kind against two pairs")
    public void fourOfAKind_TwoPairs() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.TWO_PAIRS);
    }
    
    @Test
    @DisplayName("Four of a kind against single pair")
    public void fourOfAKind_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Four of a kind against high card")
    public void fourOfAKind_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Full house against flush")
    public void fullHouse_Flush() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, UniqueHands.FLUSH);
    }
    
    @Test
    @DisplayName("Full house against straight")
    public void fullHouse_Straight() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, UniqueHands.STRAIGHT);
    }
    
    @Test
    @DisplayName("Full house against three of a kind")
    public void fullHouse_ThreeOfAKind() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, UniqueHands.THREE_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Full house against two pairs")
    public void fullHouse_TwoPairs() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, UniqueHands.TWO_PAIRS);
    }
    
    @Test
    @DisplayName("Full house against single pair")
    public void fullHouse_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Full house against high card")
    public void fullHouse_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Flush against Straight")
    public void flush_Straight() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, UniqueHands.STRAIGHT);
    }
    
    @Test
    @DisplayName("Flush against Three of a kind")
    public void flush_ThreeOfAKind() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, UniqueHands.THREE_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Flush against Two pairs")
    public void flush_TwoPairs() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, UniqueHands.TWO_PAIRS);
    }
    
    @Test
    @DisplayName("Flush against single pair")
    public void flush_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Flush against high card")
    public void flush_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Straight against Three of a kind")
    public void straight_ThreeOfAKind() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT, UniqueHands.THREE_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Straight against Two pairs")
    public void straight_TwoPairs() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT, UniqueHands.TWO_PAIRS);
    }
    
    @Test
    @DisplayName("Straight against single pair")
    public void straight_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Straight against High card")
    public void straight_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Three of a kind against Two Pairs")
    public void threeOfAKind_TwoPairs() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.THREE_OF_A_KIND, UniqueHands.TWO_PAIRS);
    }
        
    @Test
    @DisplayName("Three of a kind against single Pair")
    public void threeOfAKind_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.THREE_OF_A_KIND, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Three of a kind against High Card")
    public void threeOfAKind_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.THREE_OF_A_KIND, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Two Pairs against single Pair")
    public void twoPairs_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.TWO_PAIRS, UniqueHands.PAIR);
    }
    
    @Test
    @DisplayName("Two Pairs against high card")
    public void twoPairs_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.TWO_PAIRS, UniqueHands.HIGH_CARD);
    }
    
    @Test
    @DisplayName("Single Pair against high card")
    public void pair_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.PAIR, UniqueHands.HIGH_CARD);
    }
    
    
    //------ Same rank test with winner ------//
    
    
    @Test
    @DisplayName("Both hands straight flush with winner")
    public void straightFlush_StraightFlush_Winner() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT_FLUSH, LosingHandsWithSameRank.STRAIGHT_FLUSH);
    }
    
    @Test
    @DisplayName("Both hands four of a kind with winner")
    public void fourOfAKind_FourOfAKind_Winner() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FOUR_OF_A_KIND, LosingHandsWithSameRank.FOUR_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Both hands full house with winner")
    public void fullHouse_FullHouse_Winner() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FULL_HOUSE, LosingHandsWithSameRank.FULL_HOUSE);
    }
    
    @Test
    @DisplayName("Both hands flush with winner (high card)")
    public void flush_Flush_Winner_HighCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, LosingHandsWithSameRank.FLUSH_HIGH_CARD);
    }
    
    @Test
    @DisplayName("Both hands flush with winner (second highest card)")
    public void flush_Flush_Winner_2ndCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, LosingHandsWithSameRank.FLUSH_2ND_CARD);
    }
    
    @Test
    @DisplayName("Both hands flush with winner (third highest card)")
    public void flush_Flush_Winner_3rdCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, LosingHandsWithSameRank.FLUSH_3RD_CARD);
    }
    
    @Test
    @DisplayName("Both hands flush with winner (fourth highest card)")
    public void flush_Flush_Winner_4thCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, LosingHandsWithSameRank.FLUSH_4TH_CARD);
    }
    
    @Test
    @DisplayName("Both hands flush with winner (lowest card)")
    public void flush_Flush_Winner_5thCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.FLUSH, LosingHandsWithSameRank.FLUSH_5TH_CARD);
    }
    
    @Test
    @DisplayName("Both hands straight with winner")
    public void straight_Straight_Winner() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.STRAIGHT, LosingHandsWithSameRank.STRAIGHT);
    }
    
    @Test
    @DisplayName("Both hands three of a kind with winner")
    public void threeOfAKind_ThreeOfAKind_Winner() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.THREE_OF_A_KIND, LosingHandsWithSameRank.THREE_OF_A_KIND);
    }
    
    @Test
    @DisplayName("Both hands two pairs with winner (higher pair)")
    public void twoPairs_twoPairs_Winner_HigherPair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.TWO_PAIRS, LosingHandsWithSameRank.TWO_PAIRS_HIGHER_PAIR);
    }
    
    @Test
    @DisplayName("Both hands two pairs with winner (lower pair)")
    public void twoPairs_twoPairs_Winner_LowerPair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.TWO_PAIRS, LosingHandsWithSameRank.TWO_PAIRS_LOWER_PAIR);
    }
    
    @Test
    @DisplayName("Both hands two pairs with winner (remaining card)")
    public void twoPairs_twoPairs_Winner_RemainingCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.TWO_PAIRS, LosingHandsWithSameRank.TWO_PAIRS_REMAINING_CARD);
    }
    
    @Test
    @DisplayName("Both hands single pair with winner (pair)")
    public void pair_Pair_Winner_Pair() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.PAIR, LosingHandsWithSameRank.PAIR_PAIR);
    }
    
    @Test
    @DisplayName("Both hands single pair with winner (highest remaining card)")
    public void pair_Pair_Winner_HighestRemaining() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.PAIR, LosingHandsWithSameRank.PAIR_REMAINING_HIGH_CARD);
    }
    
    @Test
    @DisplayName("Both hands single pair with winner (second highest remaining card)")
    public void pair_Pair_Winner_2ndHighestRemaining() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.PAIR, LosingHandsWithSameRank.PAIR_REMAINING_2ND_CARD);
    }
    
    @Test
    @DisplayName("Both hands single pair with winner (lowest remaining card)")
    public void pair_Pair_Winner_3rdHighestRemaining() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.PAIR, LosingHandsWithSameRank.PAIR_REMAINING_3RD_CARD);
    }
    
    @Test
    @DisplayName("Both hands high card with winner (highest card)")
    public void highCard_HighCard_Winner_HighestCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.HIGH_CARD, LosingHandsWithSameRank.HIGH_CARD_HIGH_CARD);
    }
    
    @Test
    @DisplayName("Both hands high card with winner (second highest card)")
    public void highCard_HighCard_Winner_2ndHighestCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.HIGH_CARD, LosingHandsWithSameRank.HIGH_CARD_2ND_CARD);
    }
    
    @Test
    @DisplayName("Both hands high card with winner (third highest card)")
    public void highCard_HighCard_Winner_3rdHighestCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.HIGH_CARD, LosingHandsWithSameRank.HIGH_CARD_3RD_CARD);
    }
    
    @Test
    @DisplayName("Both hands high card with winner (fourth highest card)")
    public void highCard_HighCard_Winner_4thHighestCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.HIGH_CARD, LosingHandsWithSameRank.HIGH_CARD_4TH_CARD);
    }
    
    @Test
    @DisplayName("Both hands high card with winner (lowest card)")
    public void highCard_HighCard_Winner_5thHighestCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysWinner(UniqueHands.HIGH_CARD, LosingHandsWithSameRank.HIGH_CARD_5TH_CARD);
    }
    
    
    //------ Same rank test with draw ------//
    
    @Test
    @DisplayName("Both hands straight flush draw")
    public void straightFlush_StraightFlush_Draw() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDraw(UniqueHands.STRAIGHT_FLUSH, DrawHandsWithSameRank.STRAIGHT_FLUSH);
    }
    
    /*Four of a kind has no draw condition*/
    
    /*Full house has no draw condition*/
    
    @Test
    @DisplayName("Both hands flush draw")
    public void flush_Flush_Draw() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDraw(UniqueHands.FLUSH, DrawHandsWithSameRank.FLUSH);
    }
    
    @Test
    @DisplayName("Both hands straight draw")
    public void straight_Straight_Draw() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDraw(UniqueHands.STRAIGHT, DrawHandsWithSameRank.STRAIGHT);
    }
    
    /*Three of a kind has no draw condition*/
    
    @Test
    @DisplayName("Both hands two pairs draw")
    public void twoPairs_TwoPairs_Draw() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDraw(UniqueHands.TWO_PAIRS, DrawHandsWithSameRank.TWO_PAIRS);
    }
    
    @Test
    @DisplayName("Both hands single pair draw")
    public void pair_Pair_Draw() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDraw(UniqueHands.PAIR, DrawHandsWithSameRank.PAIR);
    }
    
    @Test
    @DisplayName("Both hands high card draw")
    public void highCard_HighCard_Draw() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDraw(UniqueHands.HIGH_CARD, DrawHandsWithSameRank.HIGH_CARD);
    }
    
    //------ Test Exceptions ------//
    
    @Test
    @DisplayName("Both hands share one card")
    public void duplicateCard_OneCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDuplicate(ExceptionHands.VALID_BASE_HAND, ExceptionHands.DUPLICATE_CARD_HAND_1_CARD);
    }
    
    @Test
    @DisplayName("Both hands share all cards (different constants)")
    public void duplicateCard_AllCards_DifferentConstant() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDuplicate(ExceptionHands.VALID_BASE_HAND, ExceptionHands.DUPLICATE_CARD_HAND_ALL_CARDS);
    }
    
    @Test
    @DisplayName("Both hands share all cards (same constant)")
    public void duplicateCard_AllCards_SameConstant() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysDuplicate(ExceptionHands.VALID_BASE_HAND, ExceptionHands.VALID_BASE_HAND);
    }
    
    @Test
    @DisplayName("One hand misses one card")
    public void oneHandMissingCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysInvalidNumberOfCards(ExceptionHands.VALID_BASE_HAND, ExceptionHands.MISSING_1_CARD_HAND);
    }
    
    @Test
    @DisplayName("One hand has one card too much")
    public void oneHandExtraCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysInvalidNumberOfCards(ExceptionHands.VALID_BASE_HAND, ExceptionHands.EXTRA_1_CARD_HAND);
    }
    
    @Test
    @DisplayName("One hand misses one card and other has one card too much")
    public void oneHandMissingCard_OoneHandExtraCard() throws InvalidNumerOfCardsException, DuplicateCardsDetected {
    	testBothWaysInvalidNumberOfCards(ExceptionHands.MISSING_1_CARD_HAND, ExceptionHands.EXTRA_1_CARD_HAND);
    }
    
    
    /**
     * Tests if winning hand is detected. One time where <code>Player1</code> has winning hand, and one time where <Code>Player2</Code> has winning hand
     * @param winningHand Hand winning against losing hand
     * @param losingHand Hand losing against winning hand
     * @throws InvalidNumerOfCardsException More or less than five cards per hand
     * @throws DuplicateCardsDetected both hands share at least one card with same value and suit
     */
    public void testBothWaysWinner(final Set<Card> winningHand, final Set<Card> losingHand) throws InvalidNumerOfCardsException, DuplicateCardsDetected
    {
    	assertEquals(Player.Player1, Control.determineWinner(winningHand, losingHand));
    	assertEquals(Player.Player2, Control.determineWinner(losingHand, winningHand));
    }
    
    /**
     * Tests if draw of both hands is detected. One time where <code>Player1</code> is <code>hand1</code> and <Player2> is <hand2>, and one time where <code>Player2</code> is <code>hand1</code> and <code>Player1</code> is <code>hand2</code>
     * @param hand1 first hand
     * @param hand2 second hand
     * @throws InvalidNumerOfCardsException More or less than five cards per hand
     * @throws DuplicateCardsDetected both hands share at least one card with same value and suit
     */
    public void testBothWaysDraw(final Set<Card> hand1, final Set<Card> hand2) throws InvalidNumerOfCardsException, DuplicateCardsDetected
    {
    	assertEquals(Player.Draw, Control.determineWinner(hand1, hand2));
    	assertEquals(Player.Draw, Control.determineWinner(hand2, hand1));
    }
    
    /**
     * Tests if at least one duplicated card is contained in hands. One time where <code>Player1</code> is <code>hand1</code> and <Player2> is <hand2>, and one time where <code>Player2</code> is <code>hand1</code> and <code>Player1</code> is <code>hand2</code>
     * @param hand1 first hand (has to share at least one card with <code>hand2</code>)
     * @param hand2 second hand (has to share at least one card with <code>hand1</code>)
     * @throws InvalidNumerOfCardsException More or less than five cards per hand
     * @throws DuplicateCardsDetected both hands share at least one card with same value and suit
     */
    public void testBothWaysDuplicate(final Set<Card> hand1, final Set<Card> hand2) throws InvalidNumerOfCardsException, DuplicateCardsDetected
    {
    	assertThrowsExactly(DuplicateCardsDetected.class, () ->Control.determineWinner(hand1, hand2));
    	assertThrowsExactly(DuplicateCardsDetected.class, () ->Control.determineWinner(hand2, hand1));
    }
    
    /**
     * Tests if at least one duplicated card is contained in hands. One time where <code>Player1</code> is <code>hand1</code> and <Player2> is <hand2>, and one time where <code>Player2</code> is <code>hand1</code> and <code>Player1</code> is <code>hand2</code>
     * @param hand1 first hand (this or <code>hand2</code> must have an invalid card number)
     * @param hand2 second hand (this or <code>hand1</code> must have an invalid card number)
     * @throws InvalidNumerOfCardsException More or less than five cards per hand
     * @throws DuplicateCardsDetected both hands share at least one card with same value and suit
     */
    public void testBothWaysInvalidNumberOfCards(final Set<Card> hand1, final Set<Card> hand2) throws InvalidNumerOfCardsException, DuplicateCardsDetected
    {
    	assertThrowsExactly(InvalidNumerOfCardsException.class, () ->Control.determineWinner(hand1, hand2));
    	assertThrowsExactly(InvalidNumerOfCardsException.class, () ->Control.determineWinner(hand2, hand1));
    }
}
