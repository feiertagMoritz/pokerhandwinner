package feiertag.moritz.poker.pokerHandWinner;

import java.util.Set;

/**
 * Hands with same rank as hand in unique card resulting in draw to hand with rank of unique card.
 * <p>
 * No duplicates only guaranteed between one hand of this class and hand with same rank of <code>UniqueHands</code>
 * <p>
 * Information: "Four of a kind", "Full house" and "Three of a kind" don't have a draw condition.
 * Therfore, no constant is provided.
 * @author Moritz Feiertag
 *
 */
public class DrawHandsWithSameRank {
	static final Set<Card> STRAIGHT_FLUSH = Set.of(	
			new Card(CardSuit.S, CardValue.K),
			new Card(CardSuit.S, CardValue.A),
			new Card(CardSuit.S, CardValue.J),
			new Card(CardSuit.S, CardValue.Q),
			new Card(CardSuit.S, CardValue.T)	);
	
// Four of a kind does not have a draw condition
	
// Full House does not have a draw condition
	
	static final Set<Card> FLUSH = Set.of(	
			new Card(CardSuit.D, CardValue.T),
			new Card(CardSuit.D, CardValue.K),
			new Card(CardSuit.D, CardValue._3),
			new Card(CardSuit.D, CardValue._4),
			new Card(CardSuit.D, CardValue.J)	);
	
	static final Set<Card> STRAIGHT = Set.of(	
			new Card(CardSuit.S, CardValue._8),
			new Card(CardSuit.H, CardValue._7),
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.H, CardValue._5),
			new Card(CardSuit.H, CardValue._4)	);
	
// Three of a kind does not have a draw condition
	
	static final Set<Card> TWO_PAIRS = Set.of(	
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.H, CardValue.J),
			new Card(CardSuit.H, CardValue.Q),
			new Card(CardSuit.S, CardValue.J),
			new Card(CardSuit.D, CardValue.Q)	);
	
	static final Set<Card> PAIR = Set.of(	
			new Card(CardSuit.H, CardValue.T),
			new Card(CardSuit.S, CardValue._4),
			new Card(CardSuit.C, CardValue._6),
			new Card(CardSuit.H, CardValue._3),
			new Card(CardSuit.S, CardValue.T)	);
	
	static final Set<Card> HIGH_CARD = Set.of(	
			new Card(CardSuit.H, CardValue._7),
			new Card(CardSuit.D, CardValue._4),
			new Card(CardSuit.H, CardValue._3),
			new Card(CardSuit.C, CardValue._5),
			new Card(CardSuit.S, CardValue.Q)	);
}
