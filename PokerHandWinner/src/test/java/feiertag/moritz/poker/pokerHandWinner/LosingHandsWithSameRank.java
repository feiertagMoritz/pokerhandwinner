package feiertag.moritz.poker.pokerHandWinner;

import java.util.Set;

/**
 * Hands with same rank as hand in unique card loose to hand with rank of unique card.
 * <p>
 * No duplicates only guaranteed between one hand of this class and hand with same rank of <code>UniqueHands</code>
 * @author Moritz Feiertag
 *
 */
public class LosingHandsWithSameRank {
	static final Set<Card> STRAIGHT_FLUSH = Set.of(	
			new Card(CardSuit.C, CardValue.K),
			new Card(CardSuit.C, CardValue.Q),
			new Card(CardSuit.C, CardValue.T),
			new Card(CardSuit.C, CardValue.J),
			new Card(CardSuit.C, CardValue._9)	);
	
	static final Set<Card> FOUR_OF_A_KIND = Set.of(	
			new Card(CardSuit.H, CardValue._8),
			new Card(CardSuit.C, CardValue.A),
			new Card(CardSuit.D, CardValue._8),
			new Card(CardSuit.C, CardValue._8),
			new Card(CardSuit.S, CardValue._8)	);
	
	static final Set<Card> FULL_HOUSE = Set.of(	
			new Card(CardSuit.H, CardValue.K),
			new Card(CardSuit.S, CardValue.K),
			new Card(CardSuit.D, CardValue.Q),
			new Card(CardSuit.S, CardValue.Q),
			new Card(CardSuit.C, CardValue.Q)	);
	
	/**
	 * Losing against high card of <code>UniqueHands.FLUSH</code>
	 */
	static final Set<Card> FLUSH_HIGH_CARD = Set.of(	
			new Card(CardSuit.H, CardValue.Q),
			new Card(CardSuit.H, CardValue.T),
			new Card(CardSuit.H, CardValue.J),
			new Card(CardSuit.H, CardValue._9),
			new Card(CardSuit.H, CardValue._7)	);
	
	/**
	 * Losing against second highest card of <code>UniqueHands.FLUSH</code
	 */
	static final Set<Card> FLUSH_2ND_CARD = Set.of(	
			new Card(CardSuit.D, CardValue._7),
			new Card(CardSuit.D, CardValue._8),
			new Card(CardSuit.D, CardValue._9),
			new Card(CardSuit.D, CardValue.K),
			new Card(CardSuit.D, CardValue.T)	);
	
	/**
	 * Losing against third highest card of <code>UniqueHands.FLUSH</code>
	 */
	static final Set<Card> FLUSH_3RD_CARD = Set.of(	
			new Card(CardSuit.C, CardValue.J),
			new Card(CardSuit.C, CardValue._9),
			new Card(CardSuit.C, CardValue._7),
			new Card(CardSuit.C, CardValue.K),
			new Card(CardSuit.C, CardValue._8)	);
	
	/**
	 * Losing against fourth highest card of <code>UniqueHands.FLUSH</code>
	 */
	static final Set<Card> FLUSH_4TH_CARD = Set.of(	
			new Card(CardSuit.H, CardValue.K),
			new Card(CardSuit.H, CardValue.J),
			new Card(CardSuit.H, CardValue.T),
			new Card(CardSuit.H, CardValue._3),
			new Card(CardSuit.H, CardValue._2)	);
	
	/**
	 * Losing against lowest card of of <code>UniqueHands.FLUSH</code>
	 */
	static final Set<Card> FLUSH_5TH_CARD = Set.of(	
			new Card(CardSuit.D, CardValue.T),
			new Card(CardSuit.D, CardValue._4),
			new Card(CardSuit.D, CardValue.J),
			new Card(CardSuit.D, CardValue._2),
			new Card(CardSuit.D, CardValue.K)	);
	
	static final Set<Card> STRAIGHT = Set.of(	
			new Card(CardSuit.C, CardValue._3),
			new Card(CardSuit.S, CardValue._4),
			new Card(CardSuit.C, CardValue._5),
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.D, CardValue._7)	);
	
	static final Set<Card> THREE_OF_A_KIND = Set.of(	
			new Card(CardSuit.H, CardValue._7),
			new Card(CardSuit.S, CardValue.A),
			new Card(CardSuit.C, CardValue._7),
			new Card(CardSuit.C, CardValue._8),
			new Card(CardSuit.D, CardValue._7)	);
	
	/**
	 * Losing against higher pair of <code>UniqueHands.TWO_PAIRS</code>
	 */
	static final Set<Card> TWO_PAIRS_HIGHER_PAIR = Set.of(	
			new Card(CardSuit.H, CardValue.J),
			new Card(CardSuit.H, CardValue.A),
			new Card(CardSuit.C, CardValue.T),
			new Card(CardSuit.S, CardValue.J),
			new Card(CardSuit.H, CardValue.T)	);
	
	/**
	 * Losing against lower pair of <code>UniqueHands.TWO_PAIRS</code>
	 */
	static final Set<Card> TWO_PAIRS_LOWER_PAIR = Set.of(	
			new Card(CardSuit.H, CardValue.Q),
			new Card(CardSuit.S, CardValue.T),
			new Card(CardSuit.D, CardValue.A),
			new Card(CardSuit.D, CardValue.T),
			new Card(CardSuit.D, CardValue.Q)	);
	
	/**
	 * Losing against remaining card of <code>UniqueHands.TWO_PAIRS</code>
	 */
	static final Set<Card> TWO_PAIRS_REMAINING_CARD = Set.of(	
			new Card(CardSuit.S, CardValue.J),
			new Card(CardSuit.D, CardValue._5),
			new Card(CardSuit.H, CardValue.J),
			new Card(CardSuit.D, CardValue.Q),
			new Card(CardSuit.H, CardValue.Q)	);
	
	/**
	 * Losing against pair of <code>UniqueHands.PAIR</code>
	 */
	static final Set<Card> PAIR_PAIR = Set.of(	
			new Card(CardSuit.C, CardValue._9),
			new Card(CardSuit.H, CardValue.K),
			new Card(CardSuit.H, CardValue.Q),
			new Card(CardSuit.D, CardValue._9),
			new Card(CardSuit.D, CardValue.A)	);
	
	/**
	 * Losing against highest remaining card of <code>UniqueHands.PAIR</code>
	 */
	static final Set<Card> PAIR_REMAINING_HIGH_CARD = Set.of(	
			new Card(CardSuit.H, CardValue.T),
			new Card(CardSuit.S, CardValue.T),
			new Card(CardSuit.H, CardValue._5),
			new Card(CardSuit.S, CardValue._4),
			new Card(CardSuit.C, CardValue._3)	);
	
	/**
	 * Losing against second highest remaining card of <code>UniqueHands.PAIR</code>
	 */
	static final Set<Card> PAIR_REMAINING_2ND_CARD = Set.of(	
			new Card(CardSuit.H, CardValue._2),
			new Card(CardSuit.H, CardValue.T),
			new Card(CardSuit.S, CardValue.T),
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.C, CardValue._3)	);
	
	/**
	 * Losing against lowest remaining card of <code>UniqueHands.PAIR</code>
	 */
	static final Set<Card> PAIR_REMAINING_3RD_CARD = Set.of(	
			new Card(CardSuit.S, CardValue._4),
			new Card(CardSuit.H, CardValue.T),
			new Card(CardSuit.C, CardValue._2),
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.S, CardValue.T)	);
	
	/**
	 * Losing against highest card of <code>UniqueHands.HIGH_CARD</code>
	 */
	static final Set<Card> HIGH_CARD_HIGH_CARD = Set.of(	
			new Card(CardSuit.S, CardValue.J),
			new Card(CardSuit.C, CardValue.T),
			new Card(CardSuit.S, CardValue._9),
			new Card(CardSuit.H, CardValue._8),
			new Card(CardSuit.D, CardValue._6)	);
	
	/**
	 * Losing against second highest card of <code>UniqueHands.HIGH_CARD</code>
	 */
	static final Set<Card> HIGH_CARD_2ND_CARD = Set.of(	
			new Card(CardSuit.S, CardValue.Q),
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.H, CardValue._5),
			new Card(CardSuit.C, CardValue._4),
			new Card(CardSuit.H, CardValue._3)	);
	
	/**
	 * Losing against third highest card of <code>UniqueHands.HIGH_CARD</code>
	 */
	static final Set<Card> HIGH_CARD_3RD_CARD = Set.of(	
			new Card(CardSuit.C, CardValue._2),
			new Card(CardSuit.D, CardValue._3),
			new Card(CardSuit.H, CardValue.Q),
			new Card(CardSuit.S, CardValue._7),
			new Card(CardSuit.S, CardValue._4)	);
	
	/**
	 * Losing against fourth highest card of <code>UniqueHands.HIGH_CARD</code>
	 */
	static final Set<Card> HIGH_CARD_4TH_CARD = Set.of(	
			new Card(CardSuit.S, CardValue.Q),
			new Card(CardSuit.S, CardValue._2),
			new Card(CardSuit.S, CardValue._7),
			new Card(CardSuit.S, CardValue._3),
			new Card(CardSuit.H, CardValue._5)	);
	
	/**
	 * Losing against lowest card of <code>UniqueHands.HIGH_CARD</code>
	 */
	static final Set<Card> HIGH_CARD_5TH_CARD = Set.of(	
			new Card(CardSuit.H, CardValue._2),
			new Card(CardSuit.S, CardValue._5),
			new Card(CardSuit.D, CardValue._4),
			new Card(CardSuit.H, CardValue._7),
			new Card(CardSuit.C, CardValue.Q)	);
}
