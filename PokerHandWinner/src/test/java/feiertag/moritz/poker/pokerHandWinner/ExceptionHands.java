package feiertag.moritz.poker.pokerHandWinner;

import java.util.Set;

/**
 * (Combining) certain hands of this class result in an Exception
 * <p>
 * Hands at this class are set to be used with other hands defined in this class.
 * @author Moritz Feiertag
 *
 */
public class ExceptionHands {
	
	/**
	 * A valid hand to test e.g. duplicate cards
	 */
	static final Set<Card> VALID_BASE_HAND = Set.of(	
			new Card(CardSuit.D, CardValue._7),
			new Card(CardSuit.H, CardValue._8),
			new Card(CardSuit.S, CardValue._9),
			new Card(CardSuit.H, CardValue._2),
			new Card(CardSuit.C, CardValue._6)	);
	
	/**
	 * One duplicate card (two of hearts) with <code>VALID_BASE_HAND</code>
	 */
	static final Set<Card> DUPLICATE_CARD_HAND_1_CARD = Set.of(	
			new Card(CardSuit.H, CardValue._2),
			new Card(CardSuit.H, CardValue._6),
			new Card(CardSuit.H, CardValue._5),
			new Card(CardSuit.H, CardValue._4),
			new Card(CardSuit.H, CardValue._3)	);
	
	/**
	 * Same five cards as <code>VALID_BASE_HAND</code>
	 */
	static final Set<Card> DUPLICATE_CARD_HAND_ALL_CARDS = Set.of(	
			new Card(CardSuit.D, CardValue._7),
			new Card(CardSuit.H, CardValue._2),
			new Card(CardSuit.S, CardValue._9),
			new Card(CardSuit.H, CardValue._8),
			new Card(CardSuit.C, CardValue._6)	);
	
	/**
	 * Hand missing one card
	 * <p>
	 * <i>Does not have a duplicate card with <code>VALID_BASE_CARD</code> or <code>EXTRA_1_CARD_HAND</code></i>
	 */
	static final Set<Card> MISSING_1_CARD_HAND = Set.of(	
			new Card(CardSuit.C, CardValue._3),
			new Card(CardSuit.D, CardValue._3),
			new Card(CardSuit.H, CardValue._3),
			new Card(CardSuit.S, CardValue._3)	);
	
	/**
	 * Hand having one card too much
	 * <p>
	 * <i>Does not have a duplicate card with <code>VALID_BASE_CARD</code> or <code>MISSING_1_CARD_HAND</code></i>
	 */
	static final Set<Card> EXTRA_1_CARD_HAND = Set.of(	
			new Card(CardSuit.D, CardValue.T),
			new Card(CardSuit.D, CardValue.J),
			new Card(CardSuit.D, CardValue.A),
			new Card(CardSuit.D, CardValue.Q),
			new Card(CardSuit.D, CardValue._9),
			new Card(CardSuit.D, CardValue.K)	);
	

}
