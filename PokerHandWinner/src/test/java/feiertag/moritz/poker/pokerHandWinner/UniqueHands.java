package feiertag.moritz.poker.pokerHandWinner;

import java.util.Set;

/**
 * Hands in this class are guaranteed not sharing a single card with another hand of this class
 * @author Moritz Feiertag
 *
 */
public class UniqueHands {
	static final Set<Card> STRAIGHT_FLUSH = Set.of(	
			new Card(CardSuit.H, CardValue.K),
			new Card(CardSuit.H, CardValue.A),
			new Card(CardSuit.H, CardValue.J),
			new Card(CardSuit.H, CardValue.Q),
			new Card(CardSuit.H, CardValue.T)	);
	
	static final Set<Card> FOUR_OF_A_KIND = Set.of(	
			new Card(CardSuit.S, CardValue._9),
			new Card(CardSuit.C, CardValue._2),
			new Card(CardSuit.H, CardValue._9),
			new Card(CardSuit.C, CardValue._9),
			new Card(CardSuit.D, CardValue._9)	);
	
	static final Set<Card> FULL_HOUSE = Set.of(	
			new Card(CardSuit.C, CardValue.A),
			new Card(CardSuit.C, CardValue.K),
			new Card(CardSuit.S, CardValue.A),
			new Card(CardSuit.D, CardValue.A),
			new Card(CardSuit.D, CardValue.K)	);
	
	static final Set<Card> FLUSH = Set.of(	
			new Card(CardSuit.S, CardValue.J),
			new Card(CardSuit.S, CardValue._4),
			new Card(CardSuit.S, CardValue._3),
			new Card(CardSuit.S, CardValue.K),
			new Card(CardSuit.S, CardValue.T)	);
	
	static final Set<Card> STRAIGHT = Set.of(	
			new Card(CardSuit.C, CardValue._6),
			new Card(CardSuit.C, CardValue._4),
			new Card(CardSuit.C, CardValue._7),
			new Card(CardSuit.S, CardValue._5),
			new Card(CardSuit.C, CardValue._8)	);
	
	static final Set<Card> THREE_OF_A_KIND = Set.of(	
			new Card(CardSuit.H, CardValue._8),
			new Card(CardSuit.S, CardValue._7),
			new Card(CardSuit.S, CardValue._8),
			new Card(CardSuit.D, CardValue._8),
			new Card(CardSuit.H, CardValue._5)	);
	
	static final Set<Card> TWO_PAIRS = Set.of(	
			new Card(CardSuit.D, CardValue.J),
			new Card(CardSuit.C, CardValue.Q),
			new Card(CardSuit.S, CardValue.Q),
			new Card(CardSuit.C, CardValue.J),
			new Card(CardSuit.S, CardValue._6)	);
	
	static final Set<Card> PAIR = Set.of(	
			new Card(CardSuit.D, CardValue._3),
			new Card(CardSuit.D, CardValue.T),
			new Card(CardSuit.D, CardValue._6),
			new Card(CardSuit.C, CardValue.T),
			new Card(CardSuit.D, CardValue._4)	);
	
	static final Set<Card> HIGH_CARD = Set.of(	
			new Card(CardSuit.H, CardValue._4),
			new Card(CardSuit.C, CardValue._3),
			new Card(CardSuit.D, CardValue._7),
			new Card(CardSuit.D, CardValue._5),
			new Card(CardSuit.D, CardValue.Q)	);
}
