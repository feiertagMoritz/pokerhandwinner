package feiertag.moritz.poker.pokerHandWinner;

public enum CardValue {
	_2(2), _3(3), _4(4), _5(5), _6(6), _7(7), _8(8), _9(9), T(10), J(11), Q(12), K(13), A(14);
	
	private final int valueAsNumber;
	
	/**
	 * Constructor of enum element of <code>CardValue</code>
	 * @param valueAsNumber Numeric value representation of card (higher number = higher value of card)
	 */
	CardValue(int valueAsNumber)
	{
		this.valueAsNumber = valueAsNumber;
	}
	
	/**
	 * Gets numeric value representation of card (higher number = higher value of card)
	 * @return Numeric representation of card
	 */
	public int getValueAsANumber() {
		return valueAsNumber;
	}
	
	/**
	 * Returns name of this enum constant omitting possible leading underscore.
	 * Enum constants without leading underscore use <code>toString()</code> in <code>Enum</code>
	 */
	@Override
	public String toString() {
		return getValueAsANumber() <10 ? String.valueOf(valueAsNumber): super.toString();
	}
	
}
