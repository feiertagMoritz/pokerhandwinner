package feiertag.moritz.poker.pokerHandWinner;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import feiertag.moritz.poker.pokerHandWinner.exceptions.DuplicateCardsDetected;
import feiertag.moritz.poker.pokerHandWinner.exceptions.InvalidNumerOfCardsException;

public class Control {
	
	private static final int REFERENCE_INDEX_3_OF_A_KIND = 2; //If hand contains three of a kind, value at index 2 must have one of them
	private final List<Card> hand1;
	private final List<Card> hand2;
	private static final int NUMBER_OF_CARDS_PER_HAND = 5;
	
	
	/**
	 * Creates an object with sorted lists of <code>hand1</code> and <code>hand2</code>
	 * @param hand1 Hand of <code>Player1</code>
	 * @param hand2 Hand of <code>Player2</code>
	 * @throws InvalidNumerOfCardsException One or both hands contain more or less than 5 cards
	 * @throws DuplicateCardsDetected One or more cards is present in <code>hand1</code> and <code>hand2</code>
	 */
	private Control(final Set<Card> hand1, final Set<Card> hand2) throws InvalidNumerOfCardsException, DuplicateCardsDetected {
		super();
		if(hand1.size() != getNumberOfCardsPerHand() ||
		   hand2.size() != getNumberOfCardsPerHand())
			throw new InvalidNumerOfCardsException(hand1.size(), hand2.size());
		this.hand1 = sortHand(hand1);
		this.hand2 = sortHand(hand2);
		final List<Card> duplicatesOnHands = new ArrayList<Card>(this.hand1);
		duplicatesOnHands.retainAll(this.hand2);
		if (!duplicatesOnHands.isEmpty())
		{
			throw new DuplicateCardsDetected(duplicatesOnHands);
		}
	}


	/**
	 * Sorts a Set of a hand and returns a List sorted after the value of a card
	 * @param HAND unsorted Set of <code>Cards</code>
	 * @return <code>List</code> containing sorted hand
	 */
	private List<Card> sortHand(final Set<Card> HAND) {
		List<Card> sortedHand = new ArrayList<Card>(HAND);
		sortedHand.sort(null);
		return sortedHand;
	}


	/**
	 * Returns the winner of two poker hands
	 * @param hand1 Hand of <code>Player1</code>
	 * @param hand2 Hand of <code>Player2</code>
	 * @return Winning player
	 * @throws InvalidNumerOfCardsException One or both hands contain more or less than 5 cards
	 * @throws DuplicateCardsDetected One or more cards is present in <code>hand1 and <code>hand2</code>
	 */
	public static Player determineWinner (final Set<Card> hand1, final Set<Card> hand2) throws InvalidNumerOfCardsException, DuplicateCardsDetected
	{
		final Control control = new Control(hand1, hand2);
		
		return control.determineWinner();
	}


	/**
	 * Returns the winner of the two poker hands set as class variables
	 * @return Winning <code>Player</code>
	 * @throws InvalidNumerOfCardsException One or both hands contain more or less than 5 cards
	 * @throws DuplicateCardsDetected One or more cards is present in <code>hand1</code> and <code>hand2</code>
	 */
	private Player determineWinner() {
		final Rank rankHand1 = getRankOfHand(hand1);
		final Rank rankHand2 = getRankOfHand(hand2);

		if(rankHand1.ordinal() > rankHand2.ordinal())
			return Player.Player1;
		if(rankHand1.ordinal() < rankHand2.ordinal())
			return Player.Player2;
		if (rankHand1.ordinal() == rankHand2.ordinal())
		{
			switch (rankHand1)
			{
			case Pair:
			case TwoPairs:
				 final List<CardValue> pairCardsValueHand1 =   getPairCardsCardValue(hand1);
				 final List<CardValue> pairCardsValueHand2 =   getPairCardsCardValue(hand2);
				 for (int i = 0; i<pairCardsValueHand1.size(); ++i)
				 {
					 if(pairCardsValueHand1.get(i).getValueAsANumber() > pairCardsValueHand2.get(i).getValueAsANumber())
						 return Player.Player1;
					 if(pairCardsValueHand1.get(i).getValueAsANumber() < pairCardsValueHand2.get(i).getValueAsANumber())
						 return Player.Player2;
				 }
				 
			case StraightFlush:
			case Flush:
			case Straight:
			case HighCard:
				return getHighCardWinner(hand1, hand2);
			case FourOfAKind:
				final int fourOfAKindHand1 = get4OfAKindCardValueAsNumber(hand1);
				final int fourOfAKindHand2 = get4OfAKindCardValueAsNumber(hand2);
				return fourOfAKindHand1 > fourOfAKindHand2 ? Player.Player1 : Player.Player2;
			case FullHouse:
			case ThreeOfAKind:
				final int threeOfAKindHand1 = get3OfAKindCardValueAsNumber(hand1);
				final int threeOfAKindHand2 = get3OfAKindCardValueAsNumber(hand2);
				return threeOfAKindHand1 > threeOfAKindHand2 ? Player.Player1 : Player.Player2;
			default:
				break;
			}
			
			throw new InternalControlException("Unimplemented rank of hand");
		}
		throw new InternalControlException("Check of winner failed");
	}

	/**
	 * Determines winner according to high card rules
	 * @param hand1 <code>Card</code>s of <code>Player1</code>
	 * @param hand2 <code>Card</code>s of <code>Player2</code>
	 * @return Winning <code>Player</code> if winner is determined, or <code>Player.Draw</code> on draw
	 */
	private Player getHighCardWinner(List<Card> hand1, List<Card> hand2) {
		if (hand1.size() != hand2.size() || hand1.size() != NUMBER_OF_CARDS_PER_HAND)
			throw new InternalControlException("Unhandeled invalid hand size");
		
		Player winner = Player.Draw;
		int i = hand1.size() - 1;
		while (i>=0) {
			if(hand1.get(i).getValue().getValueAsANumber() > hand2.get(i).getValue().getValueAsANumber()) {
				winner = Player.Player1;
				break;
			}
			if(hand1.get(i).getValue().getValueAsANumber() < hand2.get(i).getValue().getValueAsANumber()) {
				winner = Player.Player2;
				break;
			}
			--i;
		}
		return winner;
	}


	/**
	 * Gets numeric value of three of a kind card of hand
	 * @param hand Hand to get the three of a kind card value
	 * @return numeric value of three of a kind cards or -1, if hand does not have three of a kind
	 */
	private int get3OfAKindCardValueAsNumber(final List<Card> hand) {
		if(!isThreeOfAKind(hand))
		{
			throw new InternalControlException("Tried to get value of three of a kind cards on hand not having three of a kind");
		}
		return hand.get(REFERENCE_INDEX_3_OF_A_KIND).getValue().getValueAsANumber();
	}


	/**
	 * Gets numeric value of four of a kind card of hand
	 * @param hand Hand to get the four of a kind card value
	 * @return numeric value of four of a kind cards or -1, if hand does not have three of a kind
	 */
	private int get4OfAKindCardValueAsNumber(final List<Card> hand) {
		if(!isFourOfAKind(hand))
		{
			throw new InternalControlException("Tried to get value of three of a kind cards on hand not having three of a kind");
		}
		return hand.get(3).getValue().getValueAsANumber(); //info: Index 1,2, and 3 would work here
		
	}


	/**
	 * Determines the rank of a hand
	 * @param hand Hand to determine rank
	 * @return Rank of hand
	 */
	private Rank getRankOfHand(final List<Card> hand) {
		if (isStraightFlush(hand))
			return Rank.StraightFlush;
		if (isFourOfAKind(hand))
			return Rank.FourOfAKind;
		if (isFullHouse(hand))
			return Rank.FullHouse;
		if (isFlush(hand))
			return Rank.Flush;
		if (isStraight(hand))
			return Rank.Straight;
		if (isThreeOfAKind(hand))
			return Rank.ThreeOfAKind;
		if (isTwoPairs(hand))
			return Rank.TwoPairs;
		if (isPair(hand))
			return Rank.Pair;
		
		return Rank.HighCard;
	}


	/**
	 * Checks if hand has two pairs
	 * @param hand Hand to check if two pairs
	 * @return <code>true</code> if hand has two pairs, otherwise <code>false</code>
	 */
	private boolean isTwoPairs(final List<Card> hand) {
		return 2 == getNumberOfPairs(hand);
	}


	/**
	 * Checks if hand has full house
	 * @param hand Hand to check if full house
	 * @return <code>true</code> if hand has full house, otherwise <code>false</code>
	 */
	private boolean isFullHouse(final List<Card> hand) {
		return isThreeOfAKind(hand) && isPair(hand);
	}


	/**
	 * Checks if hand has single pair
	 * @param hand Hand to check if single pair
	 * @return <code>true</code> if hand has exactly one pair, otherwise <code>false</code>
	 */
	private boolean isPair(final List<Card> hand) {
		return 1 == getNumberOfPairs(hand);
	}


	/**
	 * Get number of pairs in hand
	 * @param hand Hand to determine number of pairs
	 * @return Number of pairs in hand
	 */
	private int getNumberOfPairs(final List<Card> hand) {
		return getPairCardsCardValue(hand).size();
	}


	/**
	 * Get CardValue of cards having a pair in hand
	 * @param hand Full set of cards in hand
	 * @return List of CardValues of cards having a pair in descending order (each card value is contained once in list)
	 */
	private List<CardValue> getPairCardsCardValue(final List<Card> hand) {
		final List<CardValue> cardValueContainingPairs = new ArrayList<CardValue>();
		final List<Card> pardProcessingList = new ArrayList<Card>(hand);
		
		//Remove cards with card value from processing list. If size of list decreases by 2 elements, a pair is found
		while (!pardProcessingList.isEmpty())
		{
			final int oldSizeProcessingList = pardProcessingList.size();
			final CardValue processedCardValue = pardProcessingList.get(oldSizeProcessingList -1).getValue();
			pardProcessingList.removeIf(card -> card.getValue() == processedCardValue);
			if(oldSizeProcessingList == pardProcessingList.size() + 2)
			{
				cardValueContainingPairs.add(processedCardValue);
			}
		}
		
		return cardValueContainingPairs;
	}


	/**
	 * Checks if hand is three of a kind
	 * @param hand Hand to check if three of a kind
	 * @return <code>true</code> if three of a kind, otherwise <code>false</code>
	 */
	private boolean isThreeOfAKind(final List<Card> hand) {
		// As cards are ordered, value at index 2 has to be the value of the three cards
		// example:
		// 7,7,7,8,9
		// 2,7,7,7,9
		// 2,3,7,7,7
		int numberOfReferenceCardsInHand = 0;
		
		for (final Card card : hand) {
			if(card.getValue() == hand.get(REFERENCE_INDEX_3_OF_A_KIND).getValue())
				++numberOfReferenceCardsInHand;
		}
		
		return 3 == numberOfReferenceCardsInHand;
	}


	/**
	 * Checks if hand is four of a kind
	 * @param hand Hand to check if four of a kind
	 * @return <code>true</code> if four of a kind, otherwise <code>false</code>
	 */
	private boolean isFourOfAKind(final List<Card> hand) {
		// As cards are ordered, cards on index 1,2, and 3 has to have the same value
		// example:
		// 2,3,3,3,3
		// 3,3,3,3,4
		boolean isFourOfAKind = true;
		for (int i = 2; i<=3; ++i)
		{
			if(hand.get(1).getValue().getValueAsANumber() != hand.get(i).getValue().getValueAsANumber())
			{
				isFourOfAKind = false;
				break;
			}
		}
		if (isFourOfAKind) //If cards on index 1,2, and 3 have the same value; either index 0 or index 4 has to have the same value as well
		{
			isFourOfAKind = hand.get(1).getValue().getValueAsANumber() == hand.get(0).getValue().getValueAsANumber() ||
							hand.get(1).getValue().getValueAsANumber() == hand.get(4).getValue().getValueAsANumber();
		}
		return isFourOfAKind;
	}


	/**
	 * Checks if hand is straight flush
	 * @param hand Hand to check if straight flush
	 * @return <code>true</code> if straight flush, otherwise <code>false</code>
	 */
	private boolean isStraightFlush(final List<Card> hand) {
		return isStraight(hand) && isFlush(hand);
	}


	/**
	 * Checks if hand is flush
	 * @param hand Hand to check if flush
	 * @return <code>true</code> if flush, otherwise <code>false</code>
	 */
	private boolean isFlush(final List<Card> hand) {
		boolean isFlush = true;
		
		for (int i = 1; i<hand.size(); ++i)
		{
			if(!hand.get(0).getSuit().equals(hand.get(i).getSuit()))
			{
				isFlush = false;
				break;
			}
		}
		
		return isFlush;
	}


	/**
	 * Checks if hand is straight
	 * @param hand Hand to check if straight
	 * @return <code>true</code> if straight, otherwise <code>false</code>
	 */
	private boolean isStraight(final List<Card> hand) {
		boolean isStraight = true;
		for (int i = 1; i<hand.size(); ++i)
		{
			if(hand.get(i-1).getValue().getValueAsANumber() + 1 !=
			   hand.get(i).getValue().getValueAsANumber())
			{
				isStraight = false;
				break;
			}
			
		}
		return isStraight;
	}


	/**
	 * Gets number of cards per hand
	 * @return number of cards of one hand (5)
	 */
	public static int getNumberOfCardsPerHand() {
		return NUMBER_OF_CARDS_PER_HAND;
	}
}
