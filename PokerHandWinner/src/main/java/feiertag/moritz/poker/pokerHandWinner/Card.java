package feiertag.moritz.poker.pokerHandWinner;


public class Card implements Comparable<Card>{
	private CardSuit suit; private CardValue value;
	
	/**
	 * Creates an instance of <code>Card</code>
	 * @param suit <code>Suit</code> of card
	 * @param value <code>Value</code> of card
	 */
	public Card(CardSuit suit, CardValue value) {
		super();
		this.suit = suit;
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		boolean superEquals =  super.equals(obj); //Checks if same object
		if(superEquals)
			return superEquals;
		else if (null!=obj){
			if (obj instanceof Card) {
				Card otherCard = (Card) obj;
				return this.getSuit().equals(otherCard.getSuit()) &&
						this.getValue().equals(otherCard.getValue());
			}
			else
				return false;
		}
		else
			return false;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(getSuit().toString()).append("/").append(getValue().toString()).toString();
	}
	
	/**
	 * Get suit of card
	 * @return suit of card
	 */
	public CardSuit getSuit() {
		return suit;
	}

	/**
	 * Get value of card
	 * @return value of card
	 */
	public CardValue getValue() {
		return value;
	}

	@Override
	public int compareTo(Card o) {
		return this.value.compareTo((o.value));
	}
	
	
	
}
