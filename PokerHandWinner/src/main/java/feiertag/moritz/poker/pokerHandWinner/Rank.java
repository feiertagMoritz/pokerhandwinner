package feiertag.moritz.poker.pokerHandWinner;

public enum Rank {
	HighCard,
	Pair,
	TwoPairs,
	ThreeOfAKind,
	Straight,
	Flush,
	FullHouse,
	FourOfAKind,
	StraightFlush
}
