**Determine winner of two poker Hands**

Determens the winner of two poker hands

*Info: A variant of poker is implemented where each player has seperate five cards.*

---

## Compare two hands

To compare two hands, you use the static method *Control.determineWinner (final Set<Card> hand1, final Set<Card> hand2)*  
*hand1* are the cards of player 1  
*hand2* are the cards of player 2  
  
The method returns an an Instance of the Enum *Player*:  
**Player1** if *player 1* has winning hand  
**Player2** if *player 2* has winning hand  
**Draw**	if its a draw

---

## Create a card

A card is defined by its value and its suit. To create a Card, use class Card. The constructor takes two arguments: *CardSuit suit, CardValue value*
  
**CardSuit** *suit* 		is the suit of the card created.  
**CardValue** *value* 		is the value of the card created.

---

## CardSuit

CardSuit is an enum containing the suits a card can have:

**C** for *clubs*  
**D** for *diamonds*  
**H** for *hearts*  
**S** for *spades*

---

## CardValue

CardValue is an enum representing the possible values of a card.

**_2** 	for *two*  
**_3** 	for *three*  
**_4** 	for *four*  
**_5** 	for *five*  
**_6** 	for *six*  
**_7** 	for *seven*  
**_8** 	for *eight*  
**_9** 	for *nine*  
**T** 	for *ten*  
**J**	for *jack*  
**Q**	for *queen*  
**K**	for *king*  
**A**	for *ace*  

---

## Exceptions

**DuplicateCardsDetected**: Is thrown if *hand1* and *hand2* both have cards with same value and suit.  
Method *getDuplicateCards()* returns list of cards having a duplicate.
  
**InvalidNumerOfCardsException**: Is thrown if one or both hands contain more or less than *5* cards.  
Method *getPlayersWithInvalidHandSize()* returns list of enum *Player* with a *hand* with more or less than *5* cards.  
Method *getSizeOfHand1()* returns number of cards of *hand1*.  
Method *getSizeOfHand2()* returns number of cards of *hand2*.

---

## Maven

  **groupId**: feiertag.moritz.poker
  **artifactId**: pokerHandWinner

---

## Appendix: Poker rules description

A poker deck contains 52 cards - each card has a suit which is one of clubs, diamonds, hearts, or spades (denoted C, D, H, and S). Each card also has a value which is one of 2, 3, 4, 5, 6, 7, 8, 9, 10, jack, queen, king, ace (denoted 2, 3, 4, 5, 6, 7, 8, 9, T, J, Q, K, A). For scoring purposes, the suits are unordered while the values are ordered as given above, with 2 being the lowest and ace the highest value.  
  
A poker hand consists of 5 cards dealt from the deck. Poker hands are ranked by the following partial order from lowest to highest.  
- High Card: Hands which do not fit any higher category are ranked by the value of their highest card. If the highest cards have the same value, the hands are ranked by the next highest, and so on.  
- Pair: 2 of the 5 cards in the hand have the same value. Hands which both contain a pair are ranked by the value of the cards forming the pair. If these values are the same, the hands are ranked by the values of the cards not forming the pair, in decreasing order.  
- Two Pairs: The hand contains 2 different pairs. Hands which both contain 2 pairs are ranked by the value of their highest pair. Hands with the same highest pair are ranked by the value of their other pair. If these values are the same the hands are ranked by the value of the remaining card.  
- Three of a Kind: Three of the cards in the hand have the same value. Hands which both contain three of a kind are ranked by the value of the 3 cards.  
- Straight: Hand contains 5 cards with consecutive values. Hands which both contain a straight are ranked by their highest card.  
- Flush: Hand contains 5 cards of the same suit. Hands which are both flushes are ranked using the rules for High Card.  
- Full House: 3 cards of the same value, with the remaining 2 cards forming a pair. Ranked by the value of the 3 cards.  
- Four of a kind: 4 cards with the same value. Ranked by the value of the 4 cards.  
- Straight flush: 5 cards of the same suit with consecutive values. Ranked by the highest card in the hand.  